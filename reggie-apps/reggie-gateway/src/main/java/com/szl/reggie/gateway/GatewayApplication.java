package com.szl.reggie.gateway;

import com.szl.reggie.auth.client.EnableAuthClient;
import com.szl.reggie.auth.server.EnableAuthServer;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
@Slf4j
@SpringBootApplication
@EnableDiscoveryClient
@EnableAuthClient
public class GatewayApplication {
    public static void main(String[] args) {
        SpringApplication.run(GatewayApplication.class,args);
        log.info("项目启动成功...");
    }
}
